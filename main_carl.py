import argparse
import os
import sys
import time
import yaml

import builtins
from pathlib import Path
from torch import nn
import torch
from utils import (
    decayed_learning_rate,
    accuracy,
    init_distributed_mode,
    fix_random_seeds,
    create_summary_writter,
    cosine_scheduler,
    AverageMeter,
    ProgressMeter,
)
from model import CARL
from optimizer import LARS
from dataset import Datasets
from losses import loss_fn

parser = argparse.ArgumentParser(description="CARL Training")
parser.add_argument("data", type=Path, metavar="DIR", help="path to dataset")
parser.add_argument(
    "--dataset-name",
    help="choose a valid dataset name",
    choices=[
        "stl10",
        "cifar100",
        "cifar10",
        "imagenet",
        "tiny_imagenet",
        "downsampled_imagenet64",
    ],
)
parser.add_argument(
    "--workers", default=4, type=int, help="number of data loader workers"
)
parser.add_argument(
    "--epochs", default=200, type=int, help="number of total training epochs",
)
parser.add_argument("--batch-size", default=256, type=int, help="mini-batch size")
parser.add_argument("--lr", default=0.6, type=float, help="base learning rate")
parser.add_argument("--min-lr", default=0.006, type=float, help="min learning rate")
parser.add_argument("--weight-decay", default=1e-6, type=float, help="weight decay")
parser.add_argument(
    "--lambda_", default=2.0, type=float, help="start value of lambda trade off",
)
parser.add_argument(
    "--min-lambda", default=1.0, type=float, help="end value for lambda trade off",
)
parser.add_argument(
    "--n-prototypes", default=400, type=int, help="number of prototypes"
)
parser.add_argument("--print-freq", default=100, type=int, help="print frequency")
parser.add_argument("--embed-dim", default=128, type=int, help="embdding dim")
parser.add_argument(
    "--start-epoch", default=0, type=int, help="epoch number to start training",
)
parser.add_argument(
    "--resume", default="", type=str, help="path to latest checkpoint (default: none)",
)
parser.add_argument(
    "--warmup-epochs",
    default=0,
    type=int,
    help="number of epochs for the linear learning-rate warm up.",
)
parser.add_argument(
    "--lambda-cooldown",
    default=40,
    type=float,
    help="number of epochs to cooldown the lambda parameters to 1.0",
)
parser.add_argument(
    "--save-checkpoint-every-n-epochs",
    default=50,
    type=float,
    help="epoch interval to save checkpoints",
)

#########################
#### dist parameters ###
#########################
parser.add_argument(
    "--dist-url",
    default="env://",
    type=str,
    help="""url used to set up distributed
                    training; see https://pytorch.org/docs/stable/distributed.html""",
)
parser.add_argument(
    "--world-size",
    default=-1,
    type=int,
    help="""
                    number of processes: it is set automatically and
                    should not be passed as argument""",
)
parser.add_argument(
    "--rank",
    default=0,
    type=int,
    help="""rank of this process:
                    it is set automatically and should not be passed as argument""",
)
parser.add_argument(
    "--local-rank",
    default=0,
    type=int,
    help="this argument is not used and should be ignored",
)


def main():
    args = parser.parse_args()
    init_distributed_mode(args)
    args.seed = 99
    fix_random_seeds(args.seed)

    writer = None
    stats_file = None
    if args.rank == 0:
        writer = create_summary_writter()
        stats_file = open(os.path.join(writer.log_dir, "stats.txt"), "a", buffering=1)
        print(" ".join(sys.argv))
        print(" ".join(sys.argv), file=stats_file)
    else:
        # suppress printing if not master
        def print_pass(*args):
            pass

        builtins.print = print_pass

    # build the  dataset
    datasetManager = Datasets(args.data)
    if args.dataset_name == "stl10":
        train_dataset = datasetManager.create_stl10_dataset()
    elif args.dataset_name == "cifar10":
        train_dataset = datasetManager.create_cifar10_dataset()
    elif args.dataset_name == "imagenet":
        train_dataset = datasetManager.create_imagenet_dataset()

    sampler = torch.utils.data.distributed.DistributedSampler(train_dataset)
    train_loader = torch.utils.data.DataLoader(
        train_dataset,
        sampler=sampler,
        batch_size=args.batch_size,
        num_workers=args.workers,
        pin_memory=True,
        drop_last=True,
    )

    # build model
    model = CARL(args)
    model = nn.SyncBatchNorm.convert_sync_batchnorm(model)

    # copy model to GPU
    model = model.cuda()

    # define optimizer
    optimizer = LARS(
        model.parameters(),
        lr=0,
        weight_decay=args.weight_decay,
        weight_decay_filter=True,
        lars_adaptation_filter=True,
    )

    # init lr scheduler
    lr_schedule = cosine_scheduler(
        args.lr * (args.batch_size * args.world_size) / 256.0,  # linear scaling rule
        args.min_lr,
        args.epochs,
        len(train_loader),
        warmup_epochs=args.warmup_epochs,
    )

    # wrap model
    model = nn.parallel.DistributedDataParallel(model, device_ids=[args.gpu])

    if args.rank == 0:
        with open(os.path.join(writer.log_dir, "metadata.txt"), "a") as f:
            yaml.dump(args, f, allow_unicode=True)
            f.write(str(model))

    # optionally resume from a checkpoint
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            if args.gpu is None:
                checkpoint = torch.load(args.resume)
            else:
                # Map model to be loaded to specified single gpu.
                loc = "cuda:{}".format(args.gpu)
                checkpoint = torch.load(args.resume, map_location=loc)
            args.start_epoch = checkpoint["epoch"]
            msg = model.load_state_dict(checkpoint["state_dict"], strict=True)
            optimizer.load_state_dict(checkpoint["optimizer"])
            print(
                "=> loaded checkpoint '{}' (epoch {}) msg: {}".format(
                    args.resume, checkpoint["epoch"], msg
                )
            )
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    torch.backends.cudnn.benchmark = True

    # init mixed precision
    scaler = torch.cuda.amp.GradScaler()

    for epoch in range(args.start_epoch, args.epochs):

        # set sampler
        train_loader.sampler.set_epoch(epoch)

        # train the network
        train(
            train_loader, model, optimizer, epoch, lr_schedule, writer, scaler, args,
        )

        if args.rank == 0 and (epoch + 1) % args.save_checkpoint_every_n_epochs == 0:
            # save checkpoint
            state = dict(
                epoch=epoch + 1,
                model=model.state_dict(),
                optimizer=optimizer.state_dict(),
            )
            torch.save(state, os.path.join(writer.log_dir, f"checkpoint_{epoch}.pth"))

    if args.rank == 0:
        # save final model
        torch.save(
            model.module.backbone.state_dict(),
            os.path.join(writer.log_dir, "resnet50.pth"),
        )


def train(train_loader, model, optimizer, epoch, lr_schedule, writer, scaler, args):
    batch_time = AverageMeter("Time", ":6.3f")
    data_time = AverageMeter("Data", ":6.3f")
    losses = AverageMeter("Loss", ":.4e")
    top1 = AverageMeter("Acc@1", ":6.2f")
    top5 = AverageMeter("Acc@5", ":6.2f")
    progress = ProgressMeter(
        len(train_loader),
        [batch_time, data_time, losses, top1, top5],
        prefix="Epoch: [{}]".format(epoch),
    )

    model.train()
    end = time.time()

    for step, ((v1, v2), _) in enumerate(train_loader, start=epoch * len(train_loader)):
        # print(v1.shape, v2.shape)
        data_time.update(time.time() - end)
        # update learning rate
        for param_group in optimizer.param_groups:
            param_group["lr"] = lr_schedule[step]

        v1 = v1.cuda(args.gpu, non_blocking=True)
        v2 = v2.cuda(args.gpu, non_blocking=True)

        optimizer.zero_grad()
        with torch.cuda.amp.autocast():
            embv1, embv2 = model(v1, v2)

            with torch.cuda.amp.autocast(enabled=False):
                consistency, entropy = loss_fn(embv1, embv2)

        lambda_scale = decayed_learning_rate(
            base_lr=args.lambda_,
            min_lr=args.min_lambda,
            decay_steps=len(train_loader) * args.lambda_cooldown,
            power=1,
            step=step,
        )
        loss = consistency + lambda_scale * entropy
        acc1, acc5 = accuracy(embv1, torch.argmax(embv2, dim=1), topk=(1, 5))
        losses.update(loss.item(), args.batch_size)
        top1.update(acc1, args.batch_size)
        top5.update(acc5, args.batch_size)

        scaler.scale(loss).backward()
        scaler.step(optimizer)
        scaler.update()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if step % args.print_freq == 0:
            progress.display(step)
            if args.rank == 0:
                writer.add_scalar("loss/entropy", entropy.item(), global_step=step)
                writer.add_scalar("loss/total", loss.item(), global_step=step)
                writer.add_scalar(
                    "loss/consistency", consistency.item(), global_step=step
                )
                writer.add_scalar("lambda_scale", lambda_scale, global_step=step)
                writer.add_scalar("top1", top1.avg, global_step=step)
                writer.add_scalar("top5", top5.avg, global_step=step)
                writer.add_scalar("lr", lr_schedule[step], global_step=step)
                writer.add_histogram(
                    f"dist/prototypes_v1", torch.argmax(embv1, dim=1), global_step=step
                )
                writer.add_histogram(
                    f"dist/prototypes_v2", torch.argmax(embv2, dim=1), global_step=step
                )


if __name__ == "__main__":
    main()
