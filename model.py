import torchvision
from torch import nn


class CARL(nn.Module):
    def __init__(self, args):
        super().__init__()
        self.args = args
        self.backbone = torchvision.models.resnet50()
        dim_mlp = self.backbone.fc.weight.shape[1]

        self.backbone.fc = nn.Sequential(
            nn.Linear(dim_mlp, dim_mlp, bias=False),
            nn.BatchNorm1d(dim_mlp),
            nn.ReLU(inplace=True),
            nn.Linear(dim_mlp, args.embed_dim),
        )

        self.assigner = nn.Linear(args.embed_dim, args.n_prototypes, bias=False)

    def forward(self, v1, v2):
        e1 = self.assigner(self.backbone(v1))
        e2 = self.assigner(self.backbone(v2))
        return e1, e2
