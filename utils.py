import torch
import shutil
import torch.distributed as dist
import math
import os
from torch.utils.tensorboard import SummaryWriter


def cosine_scheduler(
    base_value, final_value, epochs, niter_per_ep, warmup_epochs=0, start_warmup_value=0
):
    warmup_iters = warmup_epochs * niter_per_ep
    warmup_schedule = torch.tensor([])
    if warmup_epochs > 0:
        warmup_schedule = torch.linspace(start_warmup_value, base_value, warmup_iters)

    iters = torch.arange(epochs * niter_per_ep - warmup_iters)
    schedule = final_value + 0.5 * (base_value - final_value) * (
        1 + torch.cos(math.pi * iters / len(iters))
    )

    schedule = torch.cat((warmup_schedule, schedule))
    assert len(schedule) == epochs * niter_per_ep
    return schedule


def decayed_learning_rate(base_lr, decay_steps, step, min_lr=0.0001, power=1.0):
    step = min(step, decay_steps)
    return ((base_lr - min_lr) * (1 - step / decay_steps) ** (power)) + min_lr


class AllReduce(torch.autograd.Function):
    @staticmethod
    def forward(ctx, x):
        if (
            dist.is_available()
            and dist.is_initialized()
            and (dist.get_world_size() > 1)
        ):
            x = x.contiguous() / dist.get_world_size()
            dist.all_reduce(x)
        return x

    @staticmethod
    def backward(ctx, grads):
        return grads


def accuracy(output, target, topk=(1,)):
    """Computes the accuracy over the k top predictions for the specified values of k"""
    maxk = max(topk)
    batch_size = target.size(0)
    _, pred = output.topk(maxk, 1, True, True)
    pred = pred.t()
    correct = pred.eq(target.reshape(1, -1).expand_as(pred))
    return [correct[:k].reshape(-1).float().sum(0) * 100.0 / batch_size for k in topk]


class ProgressMeter(object):
    def __init__(self, num_batches, meters, prefix=""):
        self.batch_fmtstr = self._get_batch_fmtstr(num_batches)
        self.meters = meters
        self.prefix = prefix

    def display(self, batch):
        entries = [self.prefix + self.batch_fmtstr.format(batch)]
        entries += [str(meter) for meter in self.meters]
        print("\t".join(entries))

    def _get_batch_fmtstr(self, num_batches):
        num_digits = len(str(num_batches // 1))
        fmt = "{:" + str(num_digits) + "d}"
        return "[" + fmt + "/" + fmt.format(num_batches) + "]"


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self, name, fmt=":f"):
        self.name = name
        self.fmt = fmt
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

    def __str__(self):
        fmtstr = "{name} {val" + self.fmt + "} ({avg" + self.fmt + "})"
        return fmtstr.format(**self.__dict__)


def init_distributed_mode(args):
    """
    Initialize the following variables:
        - world_size
        - rank
    """
    # multi-GPU job (local or multi-node) - jobs started with torch.distributed.launch
    # read environment variables
    args.rank = int(os.environ["RANK"])
    args.world_size = int(os.environ["WORLD_SIZE"])

    # prepare distributed
    dist.init_process_group(
        backend="nccl",
        init_method=args.dist_url,
        world_size=args.world_size,
        rank=args.rank,
    )

    # set cuda device
    args.gpu = args.rank % torch.cuda.device_count()
    torch.cuda.set_device(args.gpu)
    return


def fix_random_seeds(seed=31):
    """
    Fix random seeds.
    """
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)


def create_summary_writter():
    writer = SummaryWriter()
    shutil.copyfile("./main_carl.py", os.path.join(writer.log_dir, "main_carl.py"))
    return writer
