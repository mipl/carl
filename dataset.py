import random
from PIL import Image, ImageFilter
import torchvision
import torchvision.transforms as transforms
from torchvision import datasets
import os


class ViewGenerator:
    def __init__(self, transform, n_views=2):
        self.n_views = n_views
        self.transform = transform

    def __call__(self, x):
        return [self.transform(x) for i in range(self.n_views)]


class GaussianBlur(object):
    def __init__(self, p):
        self.p = p

    def __call__(self, img):
        if random.random() < self.p:
            sigma = random.random() * 1.9 + 0.1
            return img.filter(ImageFilter.GaussianBlur(sigma))
        else:
            return img


class Datasets:
    def __init__(self, root):
        self.root = root
        # default values for normalization
        self.mean = (0.5, 0.5, 0.5)
        self.std = (0.5, 0.5, 0.5)

    def create_cifar10_dataset(self):
        print("=> requesting the CIFAR-10 dataset.")
        self.mean = (0.4914, 0.4822, 0.4465)
        self.std = (0.2023, 0.1994, 0.2010)
        self.scale = [0.3, 1.0]
        self.size = 32
        augmentations = self._get_color_distortions()
        train_dataset = torchvision.datasets.CIFAR10(
            root=self.root,
            split="train",
            download=True,
            transform=ViewGenerator(augmentations),
        )
        return train_dataset

    def create_cifar100_dataset(self):
        pass

    def create_stl10_dataset(self):
        print("=> requesting the STL-10 dataset.")
        self.scale = [0.3, 1.0]
        self.size = 96
        augmentations = self._get_color_distortions(
            self.size, self.scale, self.mean, self.std
        )
        train_dataset = torchvision.datasets.STL10(
            root=self.root,
            split="unlabeled",
            download=True,
            transform=ViewGenerator(augmentations),
        )
        return train_dataset

    def create_imagenet_dataset(self):
        print("=> requesting the ImageNet dataset")
        self.mean = (0.485, 0.456, 0.406)
        self.std = (0.229, 0.224, 0.225)
        self.scale = [0.1, 1.0]
        self.size = 224
        augmentations = self._get_color_distortions(
            self.size, self.scale, self.mean, self.std
        )
        train_dataset = datasets.ImageFolder(
            os.path.join(self.root, "ILSVRC2012", "train"), ViewGenerator(augmentations)
        )
        return train_dataset

    @staticmethod
    def _get_color_distortions(size, scale, mean, std):
        return transforms.Compose(
            [
                transforms.RandomResizedCrop(
                    size, interpolation=Image.BICUBIC, scale=scale
                ),
                transforms.RandomHorizontalFlip(p=0.5),
                transforms.RandomApply(
                    [
                        transforms.ColorJitter(
                            brightness=0.8, contrast=0.8, saturation=0.8, hue=0.4
                        )
                    ],
                    p=0.8,
                ),
                transforms.RandomGrayscale(p=0.2),
                GaussianBlur(p=0.5),
                transforms.ToTensor(),
                transforms.Normalize(mean, std),
            ]
        )
