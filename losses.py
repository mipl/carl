import torch
from utils import AllReduce


def loss_fn(embv1, embv2):
    embv1 = torch.softmax(embv1, dim=-1)
    embv2 = torch.softmax(embv2, dim=-1)

    consistency = 0.5 * cluster_loss(embv1, embv2.detach())
    consistency += 0.5 * cluster_loss(embv2, embv1.detach())

    entropy = kl_div(AllReduce.apply(torch.mean(embv1, dim=0)))
    entropy += kl_div(AllReduce.apply(torch.mean(embv2, dim=0)))

    return consistency, entropy


def cluster_loss(inputs, targets, EPS=1e-12):
    assert inputs.shape == targets.shape
    assert inputs.requires_grad == True
    assert targets.requires_grad == False

    loss = torch.einsum("nc,nc->n", [inputs, targets])
    loss = -torch.log(loss + EPS).mean()
    return loss


def kl_div(avg_probs, EPS=1e-12):
    return torch.sum(torch.log(avg_probs ** (avg_probs) + EPS))
