## PyTorch official implementation of Representation Learning via Consistent Assignment of Views to Clusters - (CARL)

Original [paper](https://arxiv.org/abs/2112.15421) published at 37th ACM/SIGAPP Symposium on Applied Computing (SAC'22): 

CARL is a self-supervised learning algorithm that combines deep clustering with joint-embedding self-supervised learning methods.

![This is an image](./assests/carl-overview.png)

### Self-supervised Pre-Training

```
python -m torch.distributed.launch --nproc_per_node=4 main_carl.py --local_rank=0 ../../../datasets --dataset-name stl10 --batch-size 64 --n-prototypes 400 --print-freq 1000 --lambda-cooldown 15

```

### Main Results

Compared with other self-supervised methods, CARL achieves comparable results across many datasets without requiring (1) large batch sizes, (2) non-differentiable algorithms such as the SinkHorn Knoop, and (3) without momentum encoders or extra data structures.

![CARL Results](./assests/carl-results.png)

### Citation

```
@article{Silva2022,
  title={Representation Learning via Consistent Assignment of Views to Clusters},
  author={Silva, Thalles and Ram{\'\i}rez Rivera, Ad{\'\i}n},
  journal={37th ACM/SIGAPP Symposium on Applied Computing (SAC'22)},
  year={2022}
}
```
